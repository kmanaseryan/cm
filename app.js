var matrix = require('matrix-js');
var math = require('mathjs');

let iterate = (A, callback) => {
    let size = A.size();
    let row = size[0];
    let col = size[1];
    for (let i = 0; i < row; i++) {
        for (let j = 0; j < col; j++) {
            callback(i, j, A(i, j));
        }
    }

}

let print = (A) => {
    console.log("")
    let size = A.size();
    let l1 = size[0];
    let l2 = size[1];
    for (let i = 0; i < l1; i++) {
        let str = "";
        for (let j = 0; j < l2; j++)
            str += A(i, j) + ",";
        console.log(str);

    }
    console.log("")
}

let createEmptyMatrix = (A) => {
    let size = A.size();
    let row = size[0];
    let col = size[1];
    let m = [];
    for (let i = 0; i < row; i++) {
        let vector = [];
        for (let j = 0; j < col; j++) {
            vector.push(0);
        }
        m.push(vector);
    }
    return matrix(m);
}

let createIdentityMatrix = (A) => {
    let size = A.size();
    let row = size[0];
    let col = size[1];
    let m = [];
    for (let i = 0; i < row; i++) {
        let vector = [];
        for (let j = 0; j < col; j++) {
            vector.push(0);
            if (i == j) vector[j] = 1;
        }
        m.push(vector);
    }
    return matrix(m);
}

let interchangeVectors = (vector, i, j) => {
    let temp = vector[i];
    vector[i] = vector[j];
    vector[j] = temp;

    return vector;
}

let interchangeRows = (A, r1Ind, r2Ind, jMin, jMax) => {
    for (let j = jMin; j < jMax; j++) {
        let prev = A(r1Ind, j);
        A = matrix(A.set(r1Ind, j).to(A(r2Ind, j)));
        A = matrix(A.set(r2Ind, j).to(prev));
    }
    return A;
}

let crouttDecAlg = (A, b) => {
    let S = [];
    let size = A.size();
    let colLength = size[1];
    let rowLength = size[0];

    let L = createEmptyMatrix(A);
    let U = createIdentityMatrix(A)

    for (let i = 0; i < rowLength; i++) {
        let max = Math.abs(A(i, 0));
        for (let j = 0; j < colLength; j++) {
            if (max < Math.abs(A(i, j)))
                max = Math.abs(A(i, j));
        }
        S[i] = max;
    }

    for (let i = 0; i < rowLength; i++) {
        for (let j = 0; j < colLength; j++) {
            if (i >= j) {
                let sum = 0;
                for (let p = 0; p < j; p++) {
                    sum += L(i, p) * U(p, j);
                }
                L = matrix(L.set(i, j).to(A(i, j) - sum));

                let max = 0,
                    maxIndex = i;
                for (let k = j; k < rowLength; k++) {
                    if (max < Math.abs(L(k, j) / S[k])) {
                        max = Math.abs(L(k, j) / S[k]);
                        maxIndex = k;
                    }
                }
                if (maxIndex != j) {
                    A = interchangeRows(A, j, maxIndex, j + 1, rowLength);
                    L = interchangeRows(L, j, maxIndex, 0, j + 1);
                    S = interchangeVectors(S, j, maxIndex);
                    b = interchangeVectors(b, j, maxIndex);
                }

            } else {
                let sum = 0;
                for (let p = 0; p < i; p++) {
                    sum += L(i, p) * U(p, j);
                }
                let num = (A(i, j) - sum) / L(i, i);
                U = matrix(U.set(i, j).to(num));
            }
        }


    }

    return {
        L,
        U
    }


}

let computeCondition =(mat)=>{
    let det = math.det(mat)
    
    let result = 0;
    for(let row of mat){
        for(let el of row){
            result += el * el;
        }
    }

    result = Math.sqrt(result)

    return result / det; 
}


var a = [
    [17, 1, -1, 2, -2, 3, -3, 4],
    [2, -16, -1, 3, -2, 1, 1, -4],
    [-1, 1, 15, 2, -1, 2, -1, 1],
    [2, 4, 1, -14, 1, 3, 4, -1],
    [1, 3, 1, -1, 13, 1, -2, 3],
    [-2, 1, 2, -1, 2, -12, -1, 1],
    [3, 4, -1, 1, 2, -2, 11, -3],
    [2, 1, 1, 1, -1, 1, -2, -10]

]

let A = matrix(a);

let b = [33, 30, -24, -30, 25, 22, -27, 18];

console.log('Problem #1 ------------------');
let {
    L,
    U
} = crouttDecAlg(A, b);
// console.log("Matrix L: ")
// print(L);
// console.log("Matrix U: ")
// print(U);

let cond = computeCondition(a);
console.log('Condition number: ' + cond);


let x = [],
    y = [];
let size = A.size();
let colLength = size[1];
let rowLength = size[0];

for (let k = 0; k < rowLength; k++) {
    let sum = 0;
    for (let j = 0; j < k; j++) {
        sum += L(k, j) * y[j];
    }
    y[k] = (b[k] - sum) / L(k, k);
}

for (let k = rowLength - 1; k >= 0; k--) {
    let sum = 0;
    for (let j = k + 1; j < colLength; j++) {
        sum += U(k, j) * x[j];
    }
    x[k] = y[k] - sum;

}

console.log('Solution x: ');
console.log(x);


// problem #2

let sorAlg = (A, b, omega, eps) => {
    let x = [
        []
    ];
    let size = A.size();
    let colLength = size[1];
    let rowLength = size[0];

    // init x for k = 0;
    for (let i = 0; i < colLength; i++) {
        x[0][i] = 0;
    }

    let stop = false;
    let k = 0;
    while (!stop) {
        stop = true;
        x[k + 1] = [];
        for (let i = 0; i < colLength; i++) {
            let sum = 0;
            for (let j = 0; j < i; j++) {
                sum += A(i, j) * x[k + 1][j]
            }
            for (let j = i + 1; j < colLength; j++) {
                sum += A(i, j) * x[k][j]
            }
            let nominator = b[i] - sum;
            x[k + 1][i] = (1 - omega) * x[k][i] + omega * nominator / A(i, i);

            // checking tolerance
            let delta = Math.abs(x[k + 1][i] - x[k][i]);
            if (delta > eps) {
                // failed
                stop = false;
            }

        }
        k++;
    }

    return {
        k,
        x
    };
}

A = matrix([
    [-4, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, -4, 1, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, -4, 1, 0, 0, 0, 0, 0, 0],
    [0, 0, 1, -4, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 1, -4, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, -4, 1, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, -4, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 1, -4, 1, 0],
    [0, 0, 0, 0, 0, 0, 0, 1, -4, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 1, -4]

])

b = [-27, -15, -15, -15, -15, -15, -15, -15, -15, -15]

let omegas = [0.8, 1.0, 1.1, 1.2, 1.5];

console.log("");
console.log('Problem #2 ------------------');
console.log("");

for(let i of omegas){
    let {k, x} = sorAlg(A, b, i, Math.pow(10, -4));
    console.log('Iteration k: ' + k);
    console.log('Tested based on the relaxation number 𝜔: ', i);
    console.log('x1 is: ' + x[k][0]);
}